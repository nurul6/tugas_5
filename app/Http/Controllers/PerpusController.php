<?php

namespace App\Http\Controllers;

use App\Models\perpus;
use Illuminate\Http\Request;

class PerpusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $perpus = $request->all();
        $Perpus = perpus::create($perpus);

        return response()->json($Perpus);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\perpus  $perpus
     * @return \Illuminate\Http\Response
     */
    public function show(perpus $perpus)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\perpus  $perpus
     * @return \Illuminate\Http\Response
     */
    public function edit(perpus $perpus)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\perpus  $perpus
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, perpus $perpus)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\perpus  $perpus
     * @return \Illuminate\Http\Response
     */
    public function destroy(perpus $perpus)
    {
        //
    }
}
